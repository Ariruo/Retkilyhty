import React, { Suspense, useState } from 'react';
import { Popup } from 'react-map-gl';
import { IconButton, CircularProgress, Button } from '@mui/material'; // Import CircularProgress from Material-UI
import '../styles/mainPopupStyles.css';
import { FaTimes } from 'react-icons/fa'; // Replace with your icon library
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MainPopupProps } from '../types/props';
import { useSelector } from 'react-redux';
import { selectToken } from '../redux/reducers/userReducer';



const LazyLoadedCoordinatecabin = React.lazy(() => import('./Coordinatescabin'));

const MainPopup: React.FC<MainPopupProps> = ({ selectedPark, setSelectedPark, distance, setUserDataReset }) => {
  if (!selectedPark || !setSelectedPark) {
    return null;
  }

  const userToken = selectToken(useSelector((state) => state.user));
  const userState = useSelector((state) => state.user);

  const handleDelete = async () => {

    const baseUrl: string = import.meta.env.VITE_BACKEND_URL || "http://localhost:9000";

    const deleteEndpoint = `${baseUrl}/api/deletePoint/${selectedPark.properties.id}`;

   
   

    try {
      const response = await fetch(deleteEndpoint, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userToken}`,
        },
      });

      if (response.ok) {
        setSelectedPark(null);
        toast.success('Kohde poistettu onnistuneesti!');
      } else {
        throw new Error(`Failed to delete park: ${response.statusText}`);
      }
    } catch (error) {
      console.error('Error deleting park:', error);
      toast.error('Kohdetta ei voitu poistaa.');
    } finally {
      setUserDataReset((prevState: boolean) => !prevState);
    }
  };





  

  return (
    selectedPark && (
      <Popup
      
        latitude={selectedPark.geometry.coordinates[1]}
        longitude={selectedPark.geometry.coordinates[0]}
        anchor="bottom"
        closeOnClick={false}
        onClose={() => {
          setSelectedPark(null);
        }}
        closeButton={false}
        className="main-popup"
      
      >
        <IconButton
          aria-label="close"
          onClick={() => {
            setSelectedPark(null);
          }}
          sx={{
            position: 'absolute',
            top: '-0px',
            right: '-0px',
            zIndex: '1',
            '&:hover': {
              transform: 'scale(0.9)',
            },
            '&:hover:focus-visible': {
              outline: '0,1px solid gray',
            },
          }}
        >
          <FaTimes className="h-5 w-5 text-black active:scale-x-90" />
        </IconButton>

        <div style={{ marginTop: '1rem' }}>
          <h2 className="text-center text-2xl font-semibold">{selectedPark.properties.name}</h2>
          <h2 className="mt-1 text-center text-small font-semibold">
            
            ({selectedPark.properties.tyyppi})
          </h2>


        </div>
        {distance && <p className="mt-1 text-center font-semibold">{distance.toFixed(2)} Km</p>}
        
        <Suspense fallback={<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' }}><CircularProgress style={{ color: 'black' }} /></div>}>
          <LazyLoadedCoordinatecabin
            latitude={selectedPark.geometry.coordinates[1]}
            longitude={selectedPark.geometry.coordinates[0]}
          />
        </Suspense>
        {selectedPark.properties.tyyppi === 'Oma kohde' && (
         <Button
         variant="contained"
         color="error"
         onClick={handleDelete}
      
         sx={{ marginTop: '4rem', display: 'block', margin: ' auto', width: '140x' }}
         size="small" // Adjust the size to small
       >
         Poista kohde
       </Button>
        )}
          <ToastContainer /> 
      </Popup>
    )
  );
};

export default MainPopup;
