import React, { useState } from 'react';
import { Button, TextField, Typography, IconButton, InputAdornment } from '@mui/material';
import { FaTimes } from 'react-icons/fa';
import Draggable from 'react-draggable';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

interface PasswordResetProps {
  setShowPasswordReset: React.Dispatch<React.SetStateAction<boolean>>;
}

const PasswordReset: React.FC<PasswordResetProps> = ({ setShowPasswordReset }) => {
  const [email, setEmail] = useState<string>('');

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const baseUrl = import.meta.env.VITE_BACKEND_URL || 'http://localhost:9000';
    const resetPasswordEndpoint = `${baseUrl}/api/reset-password`;

    try {
      const response = await fetch(resetPasswordEndpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });

      if (response.ok) {
        // Password reset request successful
        setShowPasswordReset(false); // Close the password reset form
        toast.success('Tarkista sähköpostisi ohjeiden saamiseksi salasanan nollaamiseksi.'); // Display success message in Finnish
      } else {
        // Password reset request failed
        toast.error('Salasanan nollaaminen epäonnistui. Yritä uudelleen.'); // Display error message in Finnish
      }
    } catch (error) {
      console.error('Virhe salasanan nollaamisen aikana:', error);
      toast.error('Tapahtui virhe. Yritä myöhemmin uudelleen.'); // Display error message in Finnish
    }
  };

  return (
    <div style={{ position: 'fixed', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', zIndex: '10' }}>
      <Draggable cancel=".form-element">
        <div>
          <form onSubmit={handleSubmit} style={{ background: 'white', padding: '15px', borderRadius: '8px', boxShadow: '0px 0px 8px rgba(0, 0, 0, 0.1)', zIndex: '10' }}>
            <IconButton
              aria-label="close"
              onClick={() => setShowPasswordReset(false)}
              onTouchStart={() => setShowPasswordReset(false)}
              style={{ position: 'absolute', top: '-5px', right: '0px', margin: '10px' }}
            >
              <FaTimes className="h-5 w-5 text-black active:scale-x-90" />
            </IconButton>
            <Typography variant="h6" gutterBottom>
              Salasanan nollaaminen
            </Typography>
            <TextField
              required
              id="email"
              name="email"
              label="Email"
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              style={{ marginBottom: '15px' }}
              className="form-element"
            />
            <Button type="submit" variant="contained" color="primary">
              Seuraava
            </Button>
          </form>
        </div>
      </Draggable>
    </div>
  );
};

export default PasswordReset;
