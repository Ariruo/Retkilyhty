import React, { useState } from 'react';
import { Button, TextField, Typography, IconButton, InputAdornment } from '@mui/material';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Draggable from 'react-draggable'; // Import Draggable component
import backgroundImage from '../../assets/backround_image.jpg'; // Import background image

interface PasswordUpdateProps {
  token: string;
}

const PasswordUpdate: React.FC<PasswordUpdateProps> = ({ token }) => {
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState<boolean>(false);
  const navigate = useNavigate(); // Initialize useNavigate

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (newPassword !== confirmPassword) {
      toast.error('New password and confirmation password do not match');
      return;
    }

    const baseUrl = import.meta.env.VITE_BACKEND_URL || 'http://localhost:9000';
    const updatePasswordEndpoint = `${baseUrl}/api/password-update/${token}`;

    try {
      const response = await fetch(updatePasswordEndpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ newPassword }),
      });

      if (response.ok) {
        // Password update successful
        toast.success('Salasana päivitetty onnistuneesti.');
        // Navigate to main page after successful update
        navigate('/'); // Navigate to the main page
      } else {
        // Password update failed
        toast.error('Salasanan päivitys epäonnistui. Yritä uudelleen.');
      }
    } catch (error) {
      console.error('Virhe salasanan päivityksen aikana:', error);
      toast.error('Tapahtui virhe. Yritä myöhemmin uudelleen.');
    }
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const toggleConfirmPasswordVisibility = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
<div
  className="relative bg-cover bg-center bg-no-repeat flex justify-center items-center min-h-screen"
  style={{
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: '100% 100%', // Adjust width and height separately
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
  }}
>

      <Draggable>
        <div style={{ marginBottom: '15px', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 8px rgba(0, 0, 0, 0.1)', backgroundColor: 'white' }}>
          <form onSubmit={handleSubmit} style={{ marginBottom: '15px' }}>
            <Typography variant="h6" style={{ paddingBottom: '20px' }}>
              Salasanan päivitys
            </Typography>
            <TextField
              required
              id="newPassword"
              name="newPassword"
              type={showPassword ? 'text' : 'password'}
              label="Uusi salasana"
              fullWidth
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
              style={{ marginBottom: '15px' }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={togglePasswordVisibility} edge="end">
                      {showPassword ? <FaEyeSlash /> : <FaEye />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              className="form-element"
            />
            <TextField
              required
              id="confirmPassword"
              name="confirmPassword"
              type={showConfirmPassword ? 'text' : 'password'}
              label="Vahvista salasana"
              fullWidth
              error={newPassword !== confirmPassword}
              helperText={newPassword !== confirmPassword && "Salasanat eivät täsmää"}
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              style={{ marginBottom: '15px' }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={toggleConfirmPasswordVisibility} edge="end">
                      {showConfirmPassword ? <FaEyeSlash /> : <FaEye />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              className="form-element"
            />
            <Button type="submit" variant="contained" color="primary" className="form-element">
              Päivitä salasana
            </Button>
          </form>
        </div>
      </Draggable>
    </div>
  );
};

export default PasswordUpdate;
