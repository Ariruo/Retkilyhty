import { useState, useEffect, useRef } from 'react';
import { ApiData } from '../types/api';
import { CustomPointFeature } from '../types/api';


function useFetchData(fetchDataFn: () => Promise<ApiData[]>, dependencies:string[] = []) {
  const [data, setData] = useState<CustomPointFeature[]>([]);
  const prevDependenciesRef = useRef<string[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const latestDependencies = dependencies;

      // Check if any of the dependencies changed
      if (!arraysEqual(prevDependenciesRef.current, latestDependencies)) {
        // Clear data when any of the dependencies change
        setData([]);
        prevDependenciesRef.current = latestDependencies; // Update prevDependenciesRef
      }

      try {
        const fetchedData = await fetchDataFn();
        const preparedData: CustomPointFeature[] = fetchedData.map((item) => ({
          type: 'Feature',
          properties: {
            cluster: false,
            id: item.id,
            name: item.name,
            tyyppi: item.tyyppi,
            maakunta: item.maakunta,
          },
          geometry: {
            type: 'Point',
            coordinates: [item.latitude, item.longitude],
          },
        }));
        setData(preparedData);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData(); // Invoke the function
  }, dependencies); // Dependencies array added as dependency

  return [data];
}

function arraysEqual(a: string[], b: string[]) {
  return JSON.stringify(a) === JSON.stringify(b);
}

export default useFetchData;