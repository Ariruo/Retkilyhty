import React from 'react';
import { BrowserRouter, Route, Routes, useParams } from 'react-router-dom';
import Navbar from './pages/navbar';
import Home from './pages/home';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';



 // Import parse function from query-string library
import PasswordUpdate from './components/PasswordUpdate'; // Import the PasswordUpdate component

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/password-update" element={<PasswordUpdateWrapper />} /> {/* Add the route for PasswordUpdate */}
          <Route path="/test" element={<Home />} />
          {/* Other routes */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

const PasswordUpdateWrapper = () => {
  const { search } = useLocation(); // Get the query string part of the URL
  const { token } = queryString.parse(search);
  
  return <PasswordUpdate token={token ?? ''} />; // Pass the token as a prop to PasswordUpdate component with a default value of an empty string
};


export default App;
