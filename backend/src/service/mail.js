import aws from 'aws-sdk';
import { config } from 'dotenv';

config();

const ses = new aws.SES({
  region: process.env.AWS_REGION, 
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

export async function sendPasswordResetEmail(email, resetToken) {

  const params = {
    Destination: {
      ToAddresses: [email]
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: `Hei,\n\nSalasanan nollaamiseksi klikkaa seuraavaa linkkiä:\n\nwww.retkilyhty.fi/password-update?token=${resetToken}\n\nTämä linkki vanhenee tunnin kuluttua.\n\nJos et pyytänyt tätä, voit ohittaa tämän sähköpostin.\n\nYstävällisin terveisin,\nRetkilyhty`
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Salasanan Nollauspyyntö'
      }
    },
    Source: process.env.EMAIL_USER
    
  };

  try {
    await ses.sendEmail(params).promise();
    console.log('Salasanan nollaus sähköposti lähetetty onnistuneesti.');
  } catch (error) {
    console.error('Virhe lähetettäessä salasanan nollaus sähköpostia:', error);
    throw error;
  }
}
