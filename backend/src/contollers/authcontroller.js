// authController.js

import bcrypt from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
import { pool } from '../service/db.js';
import { sendPasswordResetEmail } from '../service/mail.js';

const jwtSecret = process.env.JWT_SECRET;

export const loginUser = async (ctx) => {
  try {
    const { username, password } = ctx.request.body;
    const user = await findUserByUsernameOrEmail(username);

    if (!user) {
      ctx.status = 401;
      ctx.body = { error: 'Invalid username or email' };
      return;
    }
    const isPasswordValid = await bcrypt.compare(password, user.password_hash);

    if (!isPasswordValid) {
      ctx.status = 401;
      ctx.body = { error: 'Invalid password' };
      return;
    }

    const token = generateJwtToken(user.id, user.username);

    ctx.body = {
      token,
      user_id: user.id,
      username: user.username,
    };
  } catch (error) {
    console.error('Error during login:', error);
    ctx.throw(500, 'Internal Server Error');
  }
};

export const registerUser = async (ctx) => {
  try {
    const { email, username, password } = ctx.request.body;

    const hashedPassword = await bcrypt.hash(password, 10);
    const userId = await insertUser(email, username, hashedPassword);

    ctx.status = 201;
    ctx.body = {
      message: 'User registered successfully!',
      userId,
      username,
    };
  } catch (error) {
    console.error('Error registering user:', error);
    ctx.throw(500, 'Internal Server Error');
  }
};

async function findUserByUsernameOrEmail(usernameOrEmail) {
  try {
    console.log('Searching for user with username or email:', usernameOrEmail); // Log the parameter

    const query = `
    SELECT id, username, email, password_hash
    FROM users
    WHERE username ILIKE $1 OR email ILIKE $1;
    
    `;
    console.log('Executing query:', query); // Log the query

    const { rows } = await pool.query(query, [usernameOrEmail]);
    console.log('Query result:', rows); // Log the query result

    return rows[0];
  } catch (error) {
    console.error('Error in findUserByUsernameOrEmail:', error); // Log any errors
    throw error;
  }
}


async function insertUser(email, username, hashedPassword) {
  const insertQuery = `
    INSERT INTO users (email, username, password_hash)
    VALUES ($1, $2, $3)
    RETURNING id;
  `;
  const { rows } = await pool.query(insertQuery, [email, username, hashedPassword]);
  return rows[0].id;
}

function generateJwtToken(userId, username) {
  return jsonwebtoken.sign({ id: userId, username }, jwtSecret, { expiresIn: '1h' });
}

function generateResetToken() {
  // Implement token generation logic using jsonwebtoken or any other library
  const token = Math.random().toString(36).substr(2) + Date.now().toString(36);
  return token;
}

// Function to save reset token in the database
async function saveResetToken(userId, token) {
  try {
    // Calculate expiration timestamp (e.g., 1 hour from now)
    const expirationTimestamp = new Date();
    expirationTimestamp.setHours(expirationTimestamp.getHours() + 1); // Expiration time: 1 hour from now

    console.log('Inserting reset token:', token);
    console.log('Inserting expiration timestamp:', expirationTimestamp);

    // Insert the token, user ID, and expiration timestamp into the database
    const insertQuery = `
      INSERT INTO password_reset_tokens (user_id, token, expiration)
      VALUES ($1, $2, $3);
    `;
    await pool.query(insertQuery, [userId, token, expirationTimestamp]);

    console.log('Reset token saved successfully.');

    // Return success
    return true;
  } catch (error) {
    // Handle error
    console.error('Error saving reset token:', error);
    throw error;
  }
}

async function findResetToken(token) {
  const query = `
    SELECT user_id, expiration
    FROM password_reset_tokens
    WHERE token = $1;
  `;
  const { rows } = await pool.query(query, [token]);
  return rows[0];
}

async function updateUserPassword(userId, newPassword) {
  const updateQuery = `
    UPDATE users
    SET password_hash = $1
    WHERE id = $2;
  `;
  await pool.query(updateQuery, [newPassword, userId]);
}

async function findEmailByResetToken(token) {
  try {
    const query = `
      SELECT u.email
      FROM users u
      JOIN password_reset_tokens prt ON u.id = prt.user_id
      WHERE prt.token = $1;
    `;
    const { rows } = await pool.query(query, [token]);
    return rows[0].email; // Return the email associated with the token
  } catch (error) {
    console.error('Error finding email by reset token:', error);
    throw error;
  }
}





export const requestPasswordReset = async (ctx) => {
  try {
    const { email } = ctx.request.body;

    const user = await findUserByUsernameOrEmail(email);
    console.log(email)

   

    if (!user) {
      ctx.status = 404;
      ctx.body = { error: 'User not found' };
      return;
    }

    // Generate a unique token for password reset
    const resetToken = generateResetToken();

    // Store the token along with user ID and expiration timestamp in the database
    await saveResetToken(user.id, resetToken);

    // Send password reset email with the token link
    await sendPasswordResetEmail(email, resetToken);

    ctx.status = 200;
    ctx.body = { message: 'Password reset email sent successfully' };
  } catch (error) {
    console.error('Error requesting password reset:', error);
    ctx.throw(500, 'Internal Server Error');
  }
};

export const resetPassword = async (ctx) => {
  try {
    const { token } = ctx.params; // Extract token from URL params

    // Find email associated with the token in the database
    const email = await findEmailByResetToken(token);
    console.log(email)

    if (!email) {
      ctx.status = 404;
      ctx.body = { error: 'Invalid or expired reset token' };
      return;
    }

    const { newPassword } = ctx.request.body;

    // Find user by email
    const user = await findUserByUsernameOrEmail(email);
    if (!user) {
      ctx.status = 404;
      ctx.body = { error: 'User not found' };
      return;
    }

    // Hash the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Update user's password in the database
    await updateUserPassword(user.id, hashedPassword);

    ctx.status = 200;
    ctx.body = { message: 'Password updated successfully' };
  } catch (error) {
    console.error('Error resetting password:', error);
    ctx.throw(500, 'Internal Server Error');
  }
};








